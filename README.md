Hello Boomtown!

This assessment was written in Laravel/React, so you're going to need to run a 'php artisan serve' to see the finished product.

Let me know if you have any questions and/or (if you'd have the time), I'd love to hear whatever feedback you have for me concerning the way this was built.

All the best,

John Conley
