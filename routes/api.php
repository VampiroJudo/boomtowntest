<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Data paths 
Route::get('/data',                      'ApiController@getBoomtownData');
Route::get('/repos',                     'ApiController@getRepos');
Route::get('/events',                    'ApiController@getEvents');
Route::get('/hooks',                     'ApiController@getHooks');
Route::get('/issues',                    'ApiController@getIssues');
Route::get('/members',                   'ApiController@getMembers');
Route::get('/members/{id}',              'ApiController@getMembersById');
Route::get('/public_members',       	 'ApiController@getPublicMembersById');