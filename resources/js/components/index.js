import React, { Component }from 'react';
import ReactDOM from 'react-dom';
import { Route, BrowserRouter, Switch } from "react-router-dom";

// Layouts
import Header from '../components/layouts/Header';
import Sidebar from '../components/layouts/Sidebar';

// Views
import BoomtownData from '../components/views/BoomtownData';
import Repos from '../components/views/Repos';
import Events from '../components/views/Events';
import Hooks from '../components/views/Hooks';
import Issues from '../components/views/Issues';
import Members from '../components/views/Members';
import PublicMembers from '../components/views/PublicMembers';

class Index extends Component {
	render() {
    return (
      <BrowserRouter>
        <div>
          <Header />
          <Sidebar /> 
          <Switch>
            <Route exact path='/' component={BoomtownData}/>
            <Route exact path='/repos' component={Repos}/>
            <Route path='/events' component={Events}/>
            <Route path='/hooks' component={Hooks}/>
            <Route exact path='/issues' component={Issues}/>
            <Route path='/members' component={Members}/>
            <Route path='/public_members' component={PublicMembers}/>
          </Switch>
        </div>
      </BrowserRouter>
		);
	}
}

export default Index;

if (document.getElementById('index')) {
    ReactDOM.render(<Index />, document.getElementById('index'));
}
