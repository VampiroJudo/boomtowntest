import React, { Component }from 'react'
import axios from 'axios';

class BoomtownData extends Component {
  state = {
    posts: [],
    myObject: {},
    errorMessage: ''
  }
  
  componentDidMount() {
    axios.get(`api/data`)
      .then(res => {
        const posts = res.data;
				console.log(res.data);
        this.setState({ myObject : res.data});
      })
      .catch(err => { 
        this.setState({errorMessage: err.message});
      })

    axios.get(`api/repos`)
      .then(res => {
        const posts = res.data;
				console.log(res.data);
        this.setState({ posts : res.data});
      })
      .catch(err => { 
        this.setState({errorMessage: err.message});
      })
  }
	render() {
	  return (
	  	<div style={mainStyle}>
				<h3>Boomtown Stats</h3>
        <p>Created at =  { this.state.myObject.created_at }</p>
        <p>Updated at = { this.state.myObject.updated_at }</p>
        <p><strong>{ this.state.myObject.updated_at > this.state.myObject.created_at ? 'UPDATED AT is older' : 'CREATED AT is later'}</strong></p>
        <br />
        <p>Total Repos in repos_url array = { this.state.posts.map(posts => <li>{posts.repos_url}</li>).length}</p>
        <p>Public Repos =  { this.state.myObject.public_repos }</p>
        <p><strong>{ this.state.posts.map(posts => <li>{posts.repos_url}</li>).length === this.state.myObject.public_repos ? 'Repo count matches ' : 'Repo count does not match'}</strong></p>
        { this.state.errorMessage &&
        <h3 className="error"> { this.state.errorMessage } </h3> }
	  	</div>
		)
	}
}

const mainStyle = {
	paddingLeft: '15%',
	marginTop: '-800px',
	listStyleType: 'none'
}

export default BoomtownData