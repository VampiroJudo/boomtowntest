import React, { Component }from 'react'
import axios from 'axios';

class Repos extends Component {
  state = {
    posts: [],
    errorMessage: ''
  }
  
  componentDidMount() {
    axios.get(`api/repos`)
      .then(res => {
        const posts = res.data;
				console.log(res.data);
        this.setState({ posts : res.data});
      })
      .catch(err => { 
        this.setState({errorMessage: err.message});
      })
  }

	render() {
	  return (
	  	<div style={mainStyle}>
				<h3>Repo Id's</h3>
        <ol>
          { this.state.posts.map(posts => <li>{posts.id}</li>)}
        </ol>
        { this.state.errorMessage &&
        <h3 className="error"> { this.state.errorMessage } </h3> }
	  	</div>
		)
	}
}

const mainStyle = {
	paddingLeft: '15%',
	marginTop: '-800px'
}

export default Repos