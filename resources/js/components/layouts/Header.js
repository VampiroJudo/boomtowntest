import React from 'react'

function Header() {
	return (
		<header style={headerStyle}>
			<h1>BoomTown</h1>
		</header>
	)
}

const headerStyle = {
	background: '#ccc',
	color: '#000',
	textAlign: 'center',
	fontFamily: 'lato',
	marginTop: '-21px',
	border: '1 solid #000'
}

export default Header;
