import React, {Component} from 'react'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { Link } from "react-router-dom";

class Sidebar extends Component {

  render() {
    return (
      <List disablePadding dense style={styleSidebar}>
        <ListItem button>
          <ListItemText style={textStyle}><Link to="/">Boomtown Data</Link></ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText style={textStyle}><Link to="/repos">Repos</Link></ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText style={textStyle}><Link to="/events">Events</Link></ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText style={textStyle}><Link to="/hooks">Hooks</Link></ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText style={textStyle}><Link to="/issues">Issues</Link></ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText style={textStyle}><Link to="/members">Members</Link></ListItemText>
        </ListItem>
        <ListItem button>
          <ListItemText style={textStyle}><Link to="/public_members">Public Members</Link></ListItemText>
        </ListItem>
      </List>
    )
  }
}  
  
const styleSidebar = {
	width: '200px',
	height: '100vh',
	backgroundColor: '#00022E',
	color: '#fff',
	marginTop: '-22px',
	fontSize: '1.08rem',
}

const textStyle = {
	fontSize: '1.5rem',
	textAlign: 'left',
	paddingLeft: '13px',
	paddingTop: '15px',
	paddBottom: '15px',
	textTransform: 'uppercase',
	fontFamily: 'lato'
}

export default Sidebar