<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\GuzzleException;

class ApiController extends Controller {

    public function getBoomtownData() {
        $client           = new Client();
        $response         = $client->request('GET', 'https://api.github.com/orgs/BoomTownROI');
        $statusCode       = $response->getStatusCode();
        $body             = $response->getBody()->getContents();
        return $statusCode === 200 ? json_decode($body, true) : $statusCode." Error";
    }

    public function getRepos() {
        $client           = new Client();
        $response         = $client->request('GET', 'https://api.github.com/orgs/BoomTownROI/repos');
        $statusCode       = $response->getStatusCode();
        $body             = $response->getBody()->getContents();
        return $statusCode === 200 ? json_decode($body, true) : $statusCode." Error";
    }

    public function getEvents() {
        $client           = new Client();
        $response         = $client->request('GET', 'https://api.github.com/orgs/BoomTownROI/events');
        $statusCode       = $response->getStatusCode();
        $body             = $response->getBody()->getContents();
        return $statusCode === 200 ? json_decode($body, true) : $statusCode." Error";
    }

    public function getHooks() {
        $client           = new Client();
        $response         = $client->request('GET', 'https://api.github.com/orgs/BoomTownROI/hooks');
        $statusCode       = $response->getStatusCode();
        $body             = $response->getBody()->getContents();
        return $statusCode === 200 ? json_decode($body, true) : $statusCode." Error";
    }   

    public function getIssues() {
        $client           = new Client();
        $response         = $client->request('GET', 'https://api.github.com/orgs/BoomTownROI/issues');
        $statusCode       = $response->getStatusCode();
        $body             = $response->getBody()->getContents();
        return $statusCode === 200 ? json_decode($body, true) : $statusCode." Error";
    }

    public function getMembers() {
        $client           = new Client();
        $response         = $client->request('GET', 'https://api.github.com/orgs/BoomTownROI/members');
        $statusCode       = $response->getStatusCode();
        $body             = $response->getBody()->getContents();
        return $statusCode === 200 ? json_decode($body, true) : $statusCode." Error";
    }

    public function getMembersById() {
        $client           = new Client();
        $response         = $client->request('GET', 'https://api.github.com/orgs/BoomTownROI/members');
        $statusCode       = $response->getStatusCode();
        $body             = $response->getBody()->getContents();
        return $statusCode === 200 ? json_decode($body, true) : $statusCode." Error";
    }

    public function getPublicMembersById() {
        $client           = new Client();
        $response         = $client->request('GET', 'https://api.github.com/orgs/BoomTownROI/public_members');
        $statusCode       = $response->getStatusCode();
        $body             = $response->getBody()->getContents();
        return $statusCode === 200 ? json_decode($body, true) : $statusCode." Error";
    }
}